# Secure.compliance360.com Form Assessment

__<https://secure.compliance360.com/ext/NudbVBCGxUZSUYMyvjLWY10LNY_szTXF>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).



## Image elements do not have `[alt]` attributes [WCAG 1.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#non-text-content) (medium impact)

Informative elements should aim for short, descriptive alternate text. Decorative elements can be ignored with an empty alt attribute. [Learn more](https://web.dev/image-alt/).

### Logo missing `alt` description 


__Visual location:__

Image missing `alt` attribute:

![Image missing alt tag](assets/logo.png)


__HTML location:__

```html
<img src="/Static/dukeuniversity/Themes/35_Header.jpg" height="40" class="header-logo-img">
```

When an image has not `alt` attribute screen readers cannot tell the user its purpose.  They will not know if it is just a logo or if it was important information they need.

#### Suggested solution:

Add an `alt` attribute with an accurate description to the image or add invisible screen reader text.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Element does not have an alt attribute
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element has no title attribute or the title attribute is empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,2,BODY,1,DIV,0,DIV,0,DIV,0,DIV,0,DIV,1,IMG`<br>
Selector:<br>
`.header-logo-img`
</details>
---



## Form elements do not have associated labels [WCAG 3.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#labels-or-instructions) (critical impact)

Labels ensure that form controls are announced properly by assistive technologies, like screen readers. [Learn more](https://web.dev/label/).



### All form elements have no `<label>` associated with them.



![form Elements missing labels](assets/form-label-probs.png)



__Example visual location:__

![Element missing label](assets/secure-compliance360-com-Metadata-Naviga-txt1c040bff-d6ff-4376-94eb-fc2ab892930.png)

__Example HTML location :__

```html
<div class="form-group">
    <label class="__label @@class@@">First Name</label>
<div>
<span viewcontrolid="48607" viewid="2603" entityfielduid="aa8e8262-e7e8-4ba4-bd75-53fe1ecd959f" dbname="FirstName" class="__clientcontrol" id="__ctrlForm_2eb41c50-7f2a-4aaf-b92d-35a8276227e811">
    <input type="text" style="margin-top: 0px; white-space: pre-wrap;" rows="1" onchange="DataText.OnChanged(this); " id="txt0315b171-feac-4c9c-a780-82233c13fcdb" class="dirty-input">
</span>
    </div>
</div>
```

There are labels and there are form elements, but none of them are associated. As a result it could be difficult for some screen reader users to fill out the form.  It is also just invalid code regardless of accessibility.

#### Suggested solution:

**Option 1:** 

Add a `for` attribute that associates itself with the label's target ID.  Based on the code above the `<input>`'s ID is a randomly generated string.  The fix for this form input/label combination would be:

```html
<label for="__ctrlForm_2eb41c50-7f2a-4aaf-b92d-35a8276227e811"  class="__label @@class@@">
```

**Option 2:**

Add an `aria-label` to the `<input>`. Based on the code above the fix would look like this:

```html
<input aria-label="First name" ... class="dirty-input">
```

However, that will leave an orphaned form `<label>` . We would need to hide that using the`aria-hidden="true"` attribute to make the invalid code hidden or not read twice by assistive technologies, like screen readers.

```html
<label aria-hidden="true" class="__label @@class@@">
```

The total solution to Option 2 would look like this:

```html
<div class="form-group">
    <label aria-hidden="true" class="__label @@class@@">First Name</label>
<div>
<span viewcontrolid="48607" viewid="2603" entityfielduid="aa8e8262-e7e8-4ba4-bd75-53fe1ecd959f" dbname="FirstName" class="__clientcontrol" id="__ctrlForm_2eb41c50-7f2a-4aaf-b92d-35a8276227e811">
    <input aria-label="First name" type="text" style="margin-top: 0px; white-space: pre-wrap;" rows="1" onchange="DataText.OnChanged(this); " id="txt0315b171-feac-4c9c-a780-82233c13fcdb" class="dirty-input">
</span>
    </div>
</div>
```

**Option 3:**

Add `aria-labelledby` to the `<input>` and point that back to the existing label, in that case the `<label>` itself would need an `id`.

```html
<div class="form-group">
    <label id="foobar" class="__label @@class@@">First Name</label>
<div>
<span viewcontrolid="48607" viewid="2603" entityfielduid="aa8e8262-e7e8-4ba4-bd75-53fe1ecd959f" dbname="FirstName" class="__clientcontrol" id="__ctrlForm_2eb41c50-7f2a-4aaf-b92d-35a8276227e811">
    <input aria-labelledby="foobar" type="text" style="margin-top: 0px; white-space: pre-wrap;" rows="1" onchange="DataText.OnChanged(this); " id="txt0315b171-feac-4c9c-a780-82233c13fcdb" class="dirty-input">
</span>
    </div>
</div>
```

**Option 4:**

Anything compliant that works with a screen reader.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Form element does not have an implicit (wrapped) &lt;label&gt;
<br>Form element does not have an explicit &lt;label&gt;
<br>Element has no title attribute or the title attribute is empty
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,2,BODY,1,DIV,0,DIV,1,DIV,2,DIV,3,DIV,9,DIV,0,DIV,0,SPAN,0,DIV,1,DIV,0,SPAN,0,INPUT`<br>
Selector:<br>
`#txt1c040bff-d6ff-4376-94eb-fc2ab8929308`
</details>

---



## `<html>` element does not have a `[lang]` attribute [WCAG 3.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#language-of-page) (low impact)

If a page doesn&#39;t specify a lang attribute, a screen reader assumes that the page is in the default language that the user chose when setting up the screen reader. If the page isn&#39;t actually in the default language, then the screen reader might not announce the page&#39;s text correctly. [Learn more](https://web.dev/html-has-lang/).



### Add `lang` attribute.

__HTML location:__

```html
<html class="k-webkit k-webkit83 js ">
```

#### Suggested solution:
Add a `lang` attribute. For websites in english use `<html lang="en">`. [Other languages references](https://www.w3schools.com/tags/ref_language_codes.asp)

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>The &lt;html&gt; element does not have a lang attribute
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML`<br>
Selector:<br>
`html`
</details>

---



## `[id]` attributes on the page are not unique [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing) (unknown impact)

The value of an id attribute must be unique to prevent other instances from being overlooked by assistive technologies. [Learn more](https://web.dev/duplicate-id/).



### Document has multiple static elements with the same id attribute 

__Visual location:__

Not visible on page.  hidden at end of `<body>`

__HTML location:__

```html
<div class="k-list-container k-popup k-group k-reset" id="filterFields-list" data-role="popup" style="position: absolute; height: auto; display: none;">
<div class="k-list-container k-popup k-group k-reset" id="filterTypes-list" data-role="popup" style="position: absolute; height: auto; display: none;">
<div class="k-list-container k-popup k-group k-reset" id="filterFields-list" data-role="popup" style="position: absolute; height: auto; display: none;">
<div class="k-list-container k-popup k-group k-reset" id="filterTypes-list" data-role="popup" style="position: absolute; height: auto; display: none;">
```

`id="filterFields-list"` and `id="filterTypes-list"` are used twice.

What is the impact of duplicate IDs on elements?  It depends on their purpose. It could be nothing, or it could make the page inoperable.  Without knowing their purpose I can't say if it is meaningless  (other than being invalid), or if it would break something.  The developers of the form will need to investigate.

### Suggested solution:

1. Check if the page needs that ID for CSS for visual reasons.
2. Check if the page needs that ID for JS for interactive behaviors.
3. If it needs the IDs edit the dependent code, then remove duplicate IDs from the HTML.



<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Document has multiple static elements with the same id attribute
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,2,BODY,3,DIV`<br>
Selector:<br>
`.k-list-container.k-popup.k-group:nth-child(4)`
</details>

---



### Document has multiple static elements with the same `id` attribute 


__Visual location:__

![Incident Intake Form element with duplicate ID](assets/intake-id.png)

__HTML location:__

```html
<span class="saig-header-page-section" style="display: inline-block; word-break: normal; word-wrap: break-word; white-space: normal;" id="text"><h2>Incident Intake Form</h2></span>
```

#### Suggested solution:

1. Check if the page needs that ID for CSS for visual reasons.
2. Check if the page needs that ID for JS for interactive behaviors.
3. If it needs the IDs edit the dependent code, then remove duplicate IDs from the HTML.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Document has multiple static elements with the same id attribute
</details>


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`5,HTML,2,BODY,1,DIV,0,DIV,1,DIV,2,DIV,3,DIV,1,SPAN,0,SPAN,0,DIV,0,SPAN`<br>
Selector:<br>
`#__ctrlForm_baf44438-b5a4-4884-9b17-b7dadde804da3 > div > .saig-header-page-section`
</details>

---



### Document has multiple static elements with the same `id` attribute

Duplicate IDs are a problem all over the site. The examples above should suffice.

---



## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics) (medium impact)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Needs work.__

Buttons have no `:hover` state.

__Visual location:__

![buttons with no hover state](assets/buttons-no-hover.png)

#### Suggested solution:

Add a distinct hover state.

```css
input[type='button']:hover {
    background: somecolor;
}
```

---






<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.